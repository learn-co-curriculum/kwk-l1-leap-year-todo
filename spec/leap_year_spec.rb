require "spec_helper"

describe "#is_leap_year?" do

  it "knows 1997 is not a leap year" do
    expect(is_leap_year?(1997)).to eql(false)
  end

  it "knows 1996 is a leap year" do
    expect(is_leap_year?(1996)).to eql(true)
  end

  it "knows 1900 is a leap year" do
    expect(is_leap_year?(1900)).to eql(false)
  end

  it "knows 2000 is a leap year" do
    expect(is_leap_year?(2000)).to eql(true)
  end
end
